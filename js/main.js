jQuery(function ($) {

    //offcanvas
    $('#offcanvas-toggler').on('click', function (event) {
        event.preventDefault();
        $('body').addClass('offcanvas');
    });
    $('<div class="offcanvas-overlay"></div>').insertBefore('.body-innerwrapper > .offcanvas-menu');
    $('.close-offcanvas, .offcanvas-overlay').on('click', function (event) {
        event.preventDefault();
        $('body').removeClass('offcanvas');
    });


    //Sticky Menu
    $(document).ready(function () {
        //show current date
        var today = new Date().toDateString();
        console.log(today);
        $("#date-section").html(today);
        $("body.sticky-header").find('#navigation-bar, #mobile-nav-bar').sticky({topSpacing: 0});
    });


    //Search
    $(".search-icon-wrapper").on('click', function () {
        $(".search-wrapper").fadeIn(200);
    });
    $("#search-close").on('click', function () {
        $(".search-wrapper").fadeOut(200);
    });


    //select menu
    (function () {
        [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
            new SelectFx(el);
        });
    })();


    //vertical tabs on hover
    $('.vertical-tabs a').hover(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    // Contact form
    var form = $('#main-contact-form');
    form.submit(function (event) {
        event.preventDefault();
        var form_status = $('<div class="form_status"></div>');
        $.ajax({
            url: $(this).attr('action'),
            beforeSend: function () {
                form.prepend(form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn());
            }
        }).done(function (data) {
            form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
        });
    });

    // nanoscroller
    (function () {
        $(".nano").nanoScroller({
            flash: true
        });

    }).call(this);

});